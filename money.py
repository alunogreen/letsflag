#!/usr/bin/python3
import bottle,sys
from bottle import route, run, request, install, static_file, template, redirect, error

import re

def check(S):
  if len(S) != 19 or S[4] != ' ' or S[9] != ' ' or S[14] != ' ':
    return False

  S = S.replace(" ", '')
  if len(S) != 16 or not S.isdigit():
    return False

  L = map(int, S)  # makes S into a sequence of integers in one step
  if sum(L) % 10 != 0:  # % calculates the remainder, which should be 0
    return False
  return True

def cc(pat):
    # check for the pattern #### #### #### #### with each '#' being a digit
    m=re.match(r'(\d{4})\s(\d{4})\s(\d{4})\s(\d{4})$', pat.strip())
    if not m:
        return False
    # join all the digits in the 4 groups matched,
    # turn into a list of ints,
    # sum and
    # return True/False if divisible by 10:
    return sum(int(c) for c in ''.join(m.groups()))%10==0


# Estilo
@route('/js/<filename>')
def js_static(filename):
    return static_file(filename, root='./bootstrap/js')

@route('/css/<filename>')
def css_static(filename):
    return static_file(filename, root='./bootstrap/css')

@route('/fonts/<filename>')
def fonts_static(filename):
  return static_file(filename, root='./bootstrap/fonts')

#Home
@bottle.route('/')
#@bottle.view('principal')
def homepage():
	estilos = ['ANON','BANKER','ESPECIALIST','ESTUDANT']
	dificuldades = ['EASY','INTERMEDIDIUM','EXPERT','ESPECIALIST']

	return bottle.template('principal',{'estilo':estilos,'dificuldade':dificuldades})

@bottle.post('/novo')
def recon():
    nome = bottle.request.forms.get('nome')
    sobrenome = bottle.request.forms.get('sobrenome')
    ncartao = bottle.request.forms.get('ncartao')
    expira = bottle.request.forms.get('expira')
    cvc = bottle.request.forms.get('cvc')
    msg = "O nome é:"+nome+" o sobrenome é:"+sobrenome+" o numero do cartão:"+ncartao+" que expira em :"+expira+" com o vencimento em : "+cvc
    return bottle.template('novo',{'username':'GreenMind','msgalert':msg})

@error(404)
def error404(error):
    msg = 'Nothing here, sorry (404)'
    return bottle.template('error',{'username':'GreenMind','msgalert':msg})

@error(500)
def error500(error):
    msg = 'Internal Server Error(500)'
    return bottle.template('error',{'username':'GreenMind','msgalert':msg})

if len(sys.argv) < 2:
    #Porta padrão é a 8082
    porta = 8082
    bottle.run(host='0.0.0.0', port = porta)
else:
    porta = sys.argv[1]
    bottle.run(host='localhost', port = porta)
